package iris;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class IrisFileHandler {
    private static final String DEFAULT_PATH = "C:\\Users\\Daniel\\Downloads\\iris.data";

    private String outputPath = null;

    private List<IrisData> irisData;

    public IrisFileHandler()
    {
        this.irisData = new ArrayList<IrisData>();
        this.readFile();
    }
    public IrisFileHandler(String outputPath)
    {
        this();
        this.outputPath = outputPath;
    }

    private void readFile()
    {
        try
        {
            RandomAccessFile reader = new RandomAccessFile(DEFAULT_PATH,"r");

            String line = null;
            while((line = reader.readLine()) != null)
                irisData.add(this.createIrisData(line));
            reader.close();
        }
        catch (IOException ex)
        {
            System.out.println("IOException: "+ex.toString());
        }
        catch (Exception ex)
        {
            System.out.println("Exception: "+ex.toString());
        }
    }
    private IrisData createIrisData(String line)
    {
        String [] splittedData = line.split(",");
        float sepalHeight = Float.parseFloat(splittedData[0]);
        float sepalWidth = Float.parseFloat(splittedData[1]);;

        float petalHeight = Float.parseFloat(splittedData[2]);;
        float petalWidth = Float.parseFloat(splittedData[3]);;

        String strIrisType = splittedData[4];

        IrisData.IrisType irisType = null;

        if ("Iris-versicolor".equals(strIrisType)) {
            irisType = IrisData.IrisType.VERSICOLOR;
        } else if ("Iris-virginica".equals(strIrisType)) {
            irisType = IrisData.IrisType.VIRGINICA;
        } else if ("Iris-setosa".equals(strIrisType)) {
            irisType = IrisData.IrisType.SETOSA;
        }

        return new IrisData(sepalHeight,sepalWidth,petalHeight,petalWidth,irisType);
    }

    private float getAvgOfType(final IrisData.IrisType irisType,String type)
    {
        float avg = 0.0f;
        List<IrisData> filtered = irisData.stream().filter(iris -> iris.getIrisType().equals(irisType)).collect(Collectors.toList());

        for (IrisData irisData : filtered)
        {
            if(type.equals("sepalHeight"))
                avg += irisData.getSepalHeight();
            if(type.equals("sepalWidth"))
                avg += irisData.getSepalWidth();
            if(type.equals("petalHeight"))
                avg += irisData.getPetalHeight();
            if(type.equals("petalWidth"))
                avg += irisData.getPetalWidth();
        }

        return avg/filtered.size();
    }

    public float getAvgSepalHeight(IrisData.IrisType irisType)
    {
        float avg = getAvgOfType(irisType,"sepalHeight");
        if(outputPath != null)
            writeData("Avg. sepal height of type="+irisType+": "+avg+" (cm)");
        return avg;
    }
    public float getAvgSepalWidth(IrisData.IrisType irisType)
    {
        float avg = getAvgOfType(irisType,"sepalWidth");
        if(outputPath != null)
            writeData("Avg. sepal height of type="+irisType+": "+avg + " (cm)");
        return avg;
    }
    public float getAvgPetalHeight(IrisData.IrisType irisType)
    {
        float avg = getAvgOfType(irisType,"petalHeight");
        if(outputPath != null)
            writeData("Avg. petal height of type="+irisType+": "+avg + " (cm)");
        return avg;
    }
    public float getAvgPetalWidth(IrisData.IrisType irisType)
    {
        float avg = getAvgOfType(irisType,"petalWidth");
        if(outputPath != null)
            writeData("Avg. petal width of type="+irisType+": "+ avg +" (cm)\n");
        return avg;
    }

    private void writeData(String data)
    {
        try
        {
            RandomAccessFile writer = new RandomAccessFile(outputPath,"rw");
            writer.seek(writer.length());
            writer.writeBytes(data+"\n");

            writer.close();
        }
        catch (IOException ex)
        {
            System.out.println("IOException: "+ex.toString());
        }
        catch (Exception ex)
        {
            System.out.println("Exception: "+ex.toString());
        }
    }
}
