package iris;

import java.util.Scanner;

public class Main {
    public static void main(String [] args)
    {
        System.out.println("Data output path: ");
        String outputPath = new Scanner(System.in).next();

        IrisFileHandler fileHandler = new IrisFileHandler(outputPath);

        float avgSepalHeightSetosa = fileHandler.getAvgSepalHeight(IrisData.IrisType.SETOSA);
        float avgSepalWidthSetosa = fileHandler.getAvgSepalWidth(IrisData.IrisType.SETOSA);
        float avgPetalHeightSetosa = fileHandler.getAvgPetalHeight(IrisData.IrisType.SETOSA);
        float avgPetalWidthSetosa = fileHandler.getAvgPetalWidth(IrisData.IrisType.SETOSA);

        float avgSepalHeightVirginica = fileHandler.getAvgSepalHeight(IrisData.IrisType.VIRGINICA);
        float avgSepalWidthVirginica = fileHandler.getAvgSepalWidth(IrisData.IrisType.VIRGINICA);
        float avgPetalHeightVirginica = fileHandler.getAvgPetalHeight(IrisData.IrisType.VIRGINICA);
        float avgPetalWidthVirginica = fileHandler.getAvgPetalWidth(IrisData.IrisType.VIRGINICA);

        float avgSepalHeightVersicolor = fileHandler.getAvgSepalHeight(IrisData.IrisType.VERSICOLOR);
        float avgSepalWidthVersicolor = fileHandler.getAvgSepalWidth(IrisData.IrisType.VERSICOLOR);
        float avgPetalHeightVersicolor = fileHandler.getAvgPetalHeight(IrisData.IrisType.VERSICOLOR);
        float avgPetalWidthVersicolor = fileHandler.getAvgPetalWidth(IrisData.IrisType.VERSICOLOR);
    }
}
