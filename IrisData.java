package iris;

public class IrisData {

    private float sepalHeight;
    private float sepalWidth;

    private float petalHeight;
    private float petalWidth;

    enum IrisType
    {
        SETOSA,
        VERSICOLOR,
        VIRGINICA
    }

    private IrisType irisType;

    public float getPetalWidth() {
        return petalWidth;
    }

    public float getPetalHeight() {
        return petalHeight;
    }

    public float getSepalWidth() {
        return sepalWidth;
    }

    public float getSepalHeight() {
        return sepalHeight;
    }

    public IrisType getIrisType() {
        return irisType;
    }

    public IrisData(float sepalHeight,float sepalWidth,float petalHeight,float petalWidth,IrisType irisType)
    {
        this.sepalHeight = sepalHeight;
        this.sepalWidth = sepalWidth;

        this.petalHeight = petalHeight;
        this.petalWidth = petalWidth;

        this.irisType = irisType;
    }

}
